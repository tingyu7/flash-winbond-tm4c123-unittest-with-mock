#pragma once

#include "gmock/gmock.h"

// namespace WinbondUnitTest {

class WinbondMock {
public:
	virtual ~WinbondMock() {}

	MOCK_METHOD3(write_page, void(uint32_t, uint32_t, uint8_t*));
	MOCK_METHOD1(block_erase_64KB, void(uint32_t));
};

// }