#include "Fixture.h"

// using namespace WinbondUnitTest;

// namespace Winbond {

void write_page(uint32_t addr, uint32_t num_byte, uint8_t *src) {
	return TestFixture::_winbondMock->write_page(addr, num_byte, src);
}

void block_erase_64KB(uint32_t addr) {
	return TestFixture::_winbondMock->block_erase_64KB(addr);
}

// }