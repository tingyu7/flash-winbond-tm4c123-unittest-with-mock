#pragma once

#include <memory>
#include "../test_src/FlashMock.h"

// namespace WinbondUnitTest {

class TestFixture: public ::testing::Test {
public:
    TestFixture() {
        _winbondMock.reset(new ::testing::NiceMock<WinbondMock>());
    }

    ~TestFixture() {
        _winbondMock.reset();
    }

    // pointer for accessing mocked library
    static std::unique_ptr<WinbondMock> _winbondMock;
};

// }