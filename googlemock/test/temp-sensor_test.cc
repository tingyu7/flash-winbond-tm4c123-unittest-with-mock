#include <cstdint>
#include "gmock/gmock.h"  // Brings in Google Mock.
#include "gtest/gtest.h"
using ::testing::Return;

// Mocked function
uint16_t i2c_readRegister(uint16_t returnValue) {
	// return returnValue;
}

// class TempSensor {
// 	public:

// 		virtual uint16_t i2c_readRegister(uint8_t registerAddress) {
// 			return 0;
// 		}
// };

class MockTempSensor {
	public:

		MOCK_METHOD1(i2c_readRegister, uint16_t(uint8_t registerAddress));
};

// API from production code
float tempSensor_getTemperature() {

    uint16_t rawValue = i2c_readRegister(0x00);

    return -100.0f + (0.2f * (float)rawValue);
}

TEST(CheckTemp, ReadMinValue) {
	// uint8_t tempRegisterAddress = 0x03;
	float expectedTemperature = -100.0f;

	// MockTempSensor tempSensor;

	// EXPECT_CALL(tempSensor, i2c_readRegister(tempRegisterAddress))
	// 	.Times(1)
	// 	.WillOnce(Return(0x0));

	float actualTemperature = tempSensor_getTemperature();

	EXPECT_FLOAT_EQ(expectedTemperature, actualTemperature);
} 