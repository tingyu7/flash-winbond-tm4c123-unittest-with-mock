#include <iostream>
#include <algorithm>
#include <memory>
#include <stdio.h>      /* printf */
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "../test_src/winbond.h"
#include "../test_src/Fixture.h"

// ****************************************************************************
//
// Winbond Write Function Call Test with Mock
//
// ****************************************************************************
class WinbondFunctionCallTest : public TestFixture {
	public:
		WinbondFunctionCallTest() {

		}
};

TEST_F(WinbondFunctionCallTest, WritePageAllignedUnderOnePage) {
	uint8_t src[128] = {0x00};

	EXPECT_CALL(*_winbondMock, write_page(256, 128, src)).Times(1);

	my_spi_write(256, 128, src);
}

TEST_F(WinbondFunctionCallTest, WritePageAllignedMoreThanOnePage) {
	uint8_t src[384] = {0x00};

	EXPECT_CALL(*_winbondMock, write_page(512, 256, src)).Times(1);
	EXPECT_CALL(*_winbondMock, write_page(768, 128, (src + 256))).Times(1);

	my_spi_write(512, 384, src);
}

TEST_F(WinbondFunctionCallTest, 
	   WritePageNotAllignedSpaceBetweenStartAndUpperPageBound) {
	uint8_t src[120] = {0x00};

	EXPECT_CALL(*_winbondMock, write_page(640, 120, src)).Times(1);

	my_spi_write(640, 120, src);
}

TEST_F(WinbondFunctionCallTest, 
	   WritePageNotAllignedNoSpaceBetweenStartAndUpperPageBound) {
	uint8_t src[512] = {0x00};

	EXPECT_CALL(*_winbondMock, write_page(128, 128, src)).Times(1);
	EXPECT_CALL(*_winbondMock, write_page(256, 256, (src + 128))).Times(1);
	EXPECT_CALL(*_winbondMock, write_page(512, 128, (src + 384))).Times(1);

	my_spi_write(128, 512, src);
}

// ****************************************************************************
//
// Winbond Erase Function Call Test with Mock
//
// ****************************************************************************
TEST_F(WinbondFunctionCallTest, BlockEraseAddressOnPageBound) {
	EXPECT_CALL(*_winbondMock, block_erase_64KB(0)).Times(1);

	my_spi_erase(0, 256);
}

TEST_F(WinbondFunctionCallTest, BlockEraseAddressNotOnPageBound1) {
	EXPECT_CALL(*_winbondMock, block_erase_64KB(128)).Times(1);
	EXPECT_CALL(*_winbondMock, block_erase_64KB(384)).Times(1);
	
	my_spi_erase(128, 512); // 512
}

TEST_F(WinbondFunctionCallTest, BlockEraseAddressNotOnPageBound2) {
	EXPECT_CALL(*_winbondMock, block_erase_64KB(128)).Times(1);
	EXPECT_CALL(*_winbondMock, block_erase_64KB(384)).Times(1);
	EXPECT_CALL(*_winbondMock, block_erase_64KB(640)).Times(1);
	
	my_spi_erase(128, 513); // 513
}
